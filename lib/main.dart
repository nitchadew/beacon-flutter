import 'package:flutter/material.dart';
import 'package:myapp/pages/Morprom/MorpromHome.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme(
            primary: Color(0xff92E8CA),
            secondary: Colors.amber,
            surface: Color(0xff0F955D),
            background: Color.fromARGB(255, 237, 248, 243),
            error: Color(0xff5DDDA6),
            onPrimary: Colors.red,
            onSecondary: Colors.deepOrange,
            onSurface: Color(0xff0F955D),
            onBackground: Color(0xff5DDDA6),
            onError: Colors.redAccent,
            brightness: Brightness.light),
        fontFamily: GoogleFonts.prompt(textStyle: TextStyle()).fontFamily,
        textTheme: const TextTheme(
          bodyText2: TextStyle(fontSize: 14.0, color: Colors.black87),
        ),
      ),
      home: MorpromHome(title: ''),
    );
  }
}
