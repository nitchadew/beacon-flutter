import 'package:flutter_beacon/flutter_beacon.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:crypto/crypto.dart';
// import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:myapp/pages/BeaconLib/BeaconResultObject.dart';

class InetBeacon {
  final String getBeaconNameURL =
      "https://beacon-track.inet.co.th/beaconinfo?code=";
  String _beaconIdentifier;
  String _beaconUUID;
  int _cachePeriodInt;
  StreamSubscription<RangingResult> _streamRanging;
  bool _wasFound = false;
  List<BeaconResultObject> _myList = [];
  Timer _clearCacheTimer;
  bool hasInternet = true;
  bool _debug;

  InetBeacon(beaconIdentifier, beaconUUID, cachePeriodInt, {debug}) {
    if (debug == null) {
      _debug = true;
    } else {
      if (debug == true) {
        _debug = true;
      } else {
        _debug = false;
      }
    }
    print("debug: " + _debug.toString());
    _beaconIdentifier = beaconIdentifier;
    _beaconUUID = beaconUUID;
    _cachePeriodInt = cachePeriodInt;
  }

  void info() {
    print("beaconIdentifier: " +
        _beaconIdentifier +
        " " +
        "beaconUUID: " +
        _beaconUUID +
        " " +
        "cachePeriod: " +
        _cachePeriodInt.toString());
  }

  void startScan() async {
    print("start scan.");

    _clearCacheTimer =
        Timer.periodic(const Duration(milliseconds: 1000), (timer) async {
      var timeNow = DateTime.now().millisecondsSinceEpoch;
      _myList.removeWhere((item) => item.expTime < timeNow);
    });

    try {
      // checking permission
      await flutterBeacon.initializeAndCheckScanning;
      // set scan period
      // flutterBeacon.setScanPeriod(500);

      final regions = <Region>[];
      if (Platform.isIOS) {
        // iOS platform
        regions.add(
            Region(identifier: _beaconIdentifier, proximityUUID: _beaconUUID));
      } else {
        // android platform
        regions.add(
            Region(identifier: _beaconIdentifier, proximityUUID: _beaconUUID));
      }

      // to start ranging beacons
      _streamRanging =
          flutterBeacon.ranging(regions).listen((RangingResult result) async {
        // print(" ---------------------------------------------------------- ");
        try {
          // if (_debug) {
          //   print("scan result");
          //   print(result.beacons);
          // }

          if (result.beacons.length > 0) {
            // _streamRanging.pause();
            if (_myList.length == 0) {
              //1st found
              // print("-----------------------im in-------------------------");
              for (int i = 0; i < result.beacons.length; i++) {
                String name = await _getBeaconName(
                    result.beacons[i].major.toString(),
                    result.beacons[i].minor.toString());
                BeaconResultObject beaconResultObject = new BeaconResultObject(
                    result.beacons[i].major.toString(),
                    result.beacons[i].minor.toString(),
                    result.beacons[i].rssi.toString(),
                    name,
                    DateTime.now().millisecondsSinceEpoch + _cachePeriodInt);
                var checkDuplicate = _myList
                    .where((item) =>
                        item.minor == result.beacons[i].minor.toString())
                    .toList();
                if (checkDuplicate.isEmpty) {
                  _myList.add(beaconResultObject);
                }
              }
            } else {
              for (int i = 0; i < result.beacons.length; i++) {
                _wasFound = false;
                for (int j = 0; j < _myList.length; j++) {
                  if (result.beacons[i].minor.toString() == _myList[j].minor) {
                    //update exp time and rssi
                    _myList[j].expTime =
                        DateTime.now().millisecondsSinceEpoch + _cachePeriodInt;
                    _myList[j].rssi = result.beacons[i].rssi.toString();
                    _wasFound = true;
                  }
                }
                if (!_wasFound) {
                  String name = await _getBeaconName(
                      result.beacons[i].major.toString(),
                      result.beacons[i].minor.toString());
                  BeaconResultObject beaconResultObject =
                      new BeaconResultObject(
                          result.beacons[i].major.toString(),
                          result.beacons[i].minor.toString(),
                          result.beacons[i].rssi.toString(),
                          name,
                          DateTime.now().millisecondsSinceEpoch +
                              _cachePeriodInt);

                  _myList.add(beaconResultObject);
                  _wasFound = false;
                }
              }
            }
          }
          hasInternet = true;
        } on HandshakeException catch (e) {
          print(e);
          print('no internet connection.');
          hasInternet = false;
        } on SocketException catch (e) {
          print(e);
          print('no internet connection.');
          hasInternet = false;
        }

        if (_debug) {
          print("scan cache result");
          if (_myList.length > 0) {
            for (var item in _myList) {
              item.showValue();
            }
          } else {
            print("[]");
          }
        }

        // _streamRanging.resume();
      });
    } on Exception catch (e) {
      print(e);
    }
  }

  void stopScan() {
    _myList = [];
    if (_streamRanging != null) {
      _streamRanging.cancel();
      _clearCacheTimer.cancel();
      print("stoped.");
    } else {}
  }

  List<BeaconResultObject> getBeaconList() {
    if (!hasInternet) {
      throw Exception('InetBeaconNoInternetException');
    } else {
      return _myList;
    }
  }

  Future<String> _getBeaconName(String major, String minor) async {
    String strForEncode = "b1cf4b7ec203:1:" + major + ":" + minor;
    String strEncoded =
        md5.convert(convert.utf8.encode(strForEncode)).toString();
    var response = await http.get(getBeaconNameURL + strEncoded);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse['site_name'];
    } else if (response.statusCode == 503) {
      return "ประตูที่ไม่มีชื่อ";
    } else if (response.statusCode == 404) {
      return "ประตูที่ไม่มีชื่อ";
    } else {
      print("Error");
      print(getBeaconNameURL + strEncoded);
      return 'Request failed with status: ${response.statusCode}.';
    }
  }

  static Future<bool> _isBluetoothOn() async {
    // print(flutterBeacon.checkLocationServicesIfEnabled
    //     .then((value) => print(value)));

    BluetoothState bluetootStat = await flutterBeacon.bluetoothState;
    if (bluetootStat.toString() == "STATE_ON") {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> _isLocaltionOn() async {
    if (await flutterBeacon.checkLocationServicesIfEnabled) {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> isBluetoothReady() async {
    bool isBluetoothPermissionGranted = await Permission.bluetooth.isGranted;
    bool isBluetoothOnVar = await _isBluetoothOn();

    if (isBluetoothPermissionGranted && isBluetoothOnVar) {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> isLocationReady() async {
    bool isLocationPermissionGranted = await Permission.location.isGranted;
    bool isLocationOnVar = await _isLocaltionOn();

    if (isLocationPermissionGranted && isLocationOnVar) {
      return true;
    } else {
      return false;
    }
  }
}
