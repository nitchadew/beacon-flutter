class BeaconResultObject {
  String major;
  String minor;
  String rssi;
  String name;
  int expTime;

  BeaconResultObject(
      String major, String minor, String rssi, String name, int expTime) {
    this.major = major;
    this.minor = minor;
    this.rssi = rssi;
    this.name = name;
    this.expTime = expTime;
  }

  void showValue() {
    print("name: " +
        name +
        "major: " +
        major +
        " " +
        "minor: " +
        minor +
        " " +
        "rssi: " +
        rssi +
        " " +
        "exp: " +
        expTime.toString());
  }
}
