import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:async';

import 'package:myapp/pages/Morprom/CustomPageRoute.dart';
import 'package:myapp/pages/Morprom/BeaconCheckin.dart';
import 'package:localstorage/localstorage.dart';

class MorpromHome extends StatefulWidget {
  const MorpromHome({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MorpromHomeState createState() => _MorpromHomeState();
}

class _MorpromHomeState extends State<MorpromHome> {
  // String pageState = 'home';
  bool showIntro;
  final LocalStorage storage = new LocalStorage('morprom_app');
  double widgetOpacity = 1.0;
  final GlobalKey<BeaconCheckinState> _beaconCheckinKey =
      GlobalKey<BeaconCheckinState>();
  Timer textfieldTimer;

  @override
  void initState() {
    getShowIntroFromLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/home_background.png'),
              fit: BoxFit.fill),
        ),
        child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 55.0, left: 20, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AnimatedOpacity(
                    opacity: widgetOpacity,
                    duration: Duration(milliseconds: 500),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('สวัสดี,',
                                style: TextStyle(
                                    fontWeight: FontWeight.w300, fontSize: 17)),
                            Text('นาย หมอพร้อม ปลอดภัยมาก',
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 23)),
                          ],
                        ),
                        Icon(
                          Icons.settings,
                          size: 30.0,
                          color: Theme.of(context).colorScheme.surface,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.only(top: 65, left: 25, right: 25),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          _circleButton(
                              'images/ใบรับรองโควิด',
                              'ใบรับรอง\nวัคซีนโควิด',
                              'ใบรับรองโควิด',
                              Text("")),
                          _circleButton('images/ผลตรวจ_atk',
                              'ผลตรวจ\nATK/RT-PCR', 'ผลตรวจ', Text("")),
                          _circleButton(
                              'images/หาสถานที่ตรวจโควิด',
                              'หาสถานที่\nตรวจโควิด',
                              'หาสถานที่ตรวจ',
                              Text("")),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(top: 25, left: 25, right: 25),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          _circleButton(
                              'images/เช็คอินผ่านมือถือ',
                              'เช็คอิน \n ผ่านมือถือ',
                              'เช็กอินผ่านมือถือ',
                              BeaconCheckin(
                                key: _beaconCheckinKey,
                                showIntro: showIntro,
                                updateShowIntroInLocalStorage:
                                    updateShowIntroInLocalStorage,
                              )),
                          _circleButton('images/ใบรับรองแพทย์',
                              'ใบรับรอง\nแพทย์', 'ใบรับรองแพทย์', Text("")),
                          _circleButton('images/บริจาคออนไลน์',
                              'บริจาค\nออนไลน์', 'บริจาค', Text("")),
                        ],
                      )),
                ],
              ),
            ),
            _draggableScrollableSheet(),
            _buttomMenu(),
          ],
        ),
      ),
    );
  }

  Widget _circleButton(String imgPath, String buttonName, String modalHeader,
      Widget modalWidget) {
    return Column(
      children: [
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            elevation: 0,
            primary: Colors.white,
            onPrimary: Colors.white,
            minimumSize: Size(73, 73),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(1000.0)),
          ),
          onPressed: () {
            setState(() {
              widgetOpacity = 0;
            });

            Timer(Duration(milliseconds: 150), () {
              showModalBottomSheet<void>(
                backgroundColor: Colors.transparent,
                barrierColor: Colors.transparent,
                isDismissible: false,
                isScrollControlled: true,
                enableDrag: false,
                context: context,
                builder: (BuildContext context) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 15, bottom: 10),
                        child: Row(
                          children: [
                            CircleAvatar(
                              backgroundColor: Color(0xff86D0B0),
                              radius: 17,
                              child: IconButton(
                                  padding: EdgeInsets.only(right: 2),
                                  icon: Icon(Icons.arrow_back_ios_new_outlined),
                                  color: Colors.white,
                                  onPressed: modalHeader == 'เช็กอินผ่านมือถือ'
                                      ? () {
                                          _beaconCheckinKey.currentState
                                              .stopScan();
                                          Navigator.of(context).pop();
                                        }
                                      : () {
                                          Navigator.of(context).pop();
                                        }),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                left: 15,
                              ),
                              child: Text(modalHeader,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 20)),
                            ),
                            modalHeader == 'เช็กอินผ่านมือถือ'
                                ? Padding(
                                    padding: EdgeInsets.only(
                                      left: 6,
                                    ),
                                    child: CircleAvatar(
                                      backgroundColor: Color(0xff86D0B0),
                                      radius: 12,
                                      child: IconButton(
                                        iconSize: 18,
                                        padding: EdgeInsets.only(right: 0),
                                        icon: Icon(Icons.question_mark),
                                        color: Colors.white,
                                        onPressed: () {
                                          _beaconCheckinKey.currentState
                                              .showIntroUpdate(true);
                                        },
                                      ),
                                    ),
                                  )
                                : Text("")
                          ],
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30.0),
                            topLeft: Radius.circular(30.0),
                          ),
                          color: Colors.white,
                        ),
                        height: MediaQuery.of(context).size.height * 0.86,
                        width: MediaQuery.of(context).size.width * 1,
                        child: modalWidget,
                      ),
                    ],
                  );
                },
              ).whenComplete(() {
                Timer(Duration(milliseconds: 200), () {
                  setState(() {
                    widgetOpacity = 1;
                  });
                });
              });
            });

            // Navigator.of(context).push(CustomPageRoute(BeaconCheckin()));
          },
          child: SizedBox(
              height: 37,
              child: Image.asset(
                imgPath + '.png',
              )),
        ),
        Padding(
          padding: EdgeInsets.only(top: 6),
          child: Text(buttonName,
              style: TextStyle(fontWeight: FontWeight.w100, fontSize: 17),
              textAlign: TextAlign.center),
        ),
      ],
    );
  }

  Widget _buttomMenu() {
    return Align(
        alignment: FractionalOffset.bottomCenter,
        child: Container(
            height: 75,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(20.0),
                topLeft: Radius.circular(20.0),
              ),
              color: Color(0xff255644),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 30.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _buttomMenuButtton("images/icon_home", "หน้าแรก"),
                  _buttomMenuButtton("images/icon_qr", "QR/สแกน"),
                  _buttomMenuButtton("images/icon_notify", "แจ้งเตือน"),
                  _buttomMenuButtton(
                      "images/icon_healthrecord", "ประวัติสุขภาพ")
                ],
              ),
            )));
  }

  Widget _buttomMenuButtton(String imgPath, String buttonName) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {},
          child: SizedBox(
            height: 35,
            child: Image.asset(
              imgPath + '.png',
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 0),
          child: Text(buttonName,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 13),
              textAlign: TextAlign.center),
        ),
      ],
    );
  }

  Widget _draggableScrollableSheet() {
    return DraggableScrollableSheet(
      initialChildSize: 0.41,
      minChildSize: 0.41,
      maxChildSize: 0.8,
      builder: (BuildContext context, myscrollController) {
        //NotificationListene for disable scrolling glow effect
        return NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (OverscrollIndicatorNotification overscroll) {
              overscroll.disallowIndicator();
              return;
            },
            child: SingleChildScrollView(
              controller: myscrollController,
              child: Container(
                  padding: EdgeInsets.only(top: 20, bottom: 75),
                  alignment: Alignment.topLeft,
                  height: MediaQuery.of(context).size.height * 0.8,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30.0),
                      topLeft: Radius.circular(30.0),
                    ),
                    color: Colors.white,
                  ),
                  child: SingleChildScrollView(
                      controller: myscrollController,
                      child: Padding(
                        padding: EdgeInsets.only(left: 20.0, right: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('หัวข้อ',
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 18)),
                            _covidCard(),
                            Padding(
                                padding: EdgeInsets.only(top: 40),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('ข่าวสาร',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 18)),
                                    Text('ดูทั้งหมด',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w300,
                                            fontSize: 16,
                                            color: Colors.black54)),
                                  ],
                                )),
                            _highLightCard(
                                "EU ยอมรับ Digital Health Pass บน 'หมอพร้อม'",
                                "เริ่มใช้ภายใน มกราคม 2565",
                                "images/news_1"),
                            _highLightCard(
                                "EU ยอมรับ Digital Health Pass บน 'หมอพร้อม'",
                                "เริ่มใช้ภายใน มกราคม 2565",
                                "images/news_1"),
                            _highLightCard(
                                "EU ยอมรับ Digital Health Pass บน 'หมอพร้อม'",
                                "เริ่มใช้ภายใน มกราคม 2565",
                                "images/news_1"),
                            _highLightCard(
                                "EU ยอมรับ Digital Health Pass บน 'หมอพร้อม'",
                                "เริ่มใช้ภายใน มกราคม 2565",
                                "images/news_1"),
                          ],
                        ),
                      ))),
            ));
      },
    );
  }

  Widget _highLightCard(String title, String subtitle, String imgPath) {
    return Padding(
      padding: EdgeInsets.only(left: 0.0, right: 0.0, top: 15),
      child: Container(
          height: MediaQuery.of(context).size.width * 0.3 - 20,
          width: MediaQuery.of(context).size.width * 1 - 40,
          decoration: BoxDecoration(
            color: Color(0xffEBFDF6),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.10),
                spreadRadius: 3,
                blurRadius: 10,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Row(
            children: [
              Expanded(
                flex: 3,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10.0),
                      topLeft: Radius.circular(10.0),
                    ),
                    image: DecorationImage(
                        image: AssetImage(imgPath + '.png'), fit: BoxFit.fill),
                  ),
                ),
              ),
              Expanded(
                flex: 7,
                child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            text: TextSpan(
                                text: title,
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 17,
                                    color: Colors.black,
                                    fontFamily: GoogleFonts.prompt(
                                            textStyle: TextStyle())
                                        .fontFamily))),
                        RichText(
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            text: TextSpan(
                                text: subtitle,
                                style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 14,
                                    color: Colors.black54,
                                    fontFamily: GoogleFonts.prompt(
                                            textStyle: TextStyle())
                                        .fontFamily)))
                      ],
                    )),
              ),
            ],
          )),
    );
  }

  Widget _covidCard() {
    return Padding(
      padding: EdgeInsets.only(left: 0.0, right: 0.0, top: 15),
      child: Container(
          height: MediaQuery.of(context).size.width * 0.3 - 20,
          width: MediaQuery.of(context).size.width * 1 - 40,
          decoration: BoxDecoration(
            color: Color(0xffEBFDF6),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.10),
                spreadRadius: 3,
                blurRadius: 10,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Padding(
            padding: EdgeInsets.all(13),
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 4,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                            height: 30,
                            child: Image.asset('images/covid_virus.png')),
                        Text('+100,000',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 36,
                                color: Color(0xffEE8D3E))),
                      ],
                    ),
                  ),
                ),
                VerticalDivider(
                  color: Colors.black26,
                  thickness: 1,
                ),
                Expanded(
                    flex: 3,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('ในประเทศ',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 13,
                                  )),
                              Text('80,000',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 24,
                                  )),
                            ],
                          ),
                        ],
                      ),
                    )),
                Expanded(
                    flex: 3,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('จากต่างประเทศ',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 13,
                                  )),
                              Text('20,000',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 24,
                                  )),
                            ],
                          ),
                        ],
                      ),
                    )),
              ],
            ),
          )),
    );
  }

  void updateShowIntroInLocalStorage() async {
    if (await storage.ready) {
      await storage.setItem('showIntro', false);
      print("updateLocalStorage");
      print(storage.getItem('showIntro'));
      setState(() {
        showIntro = storage.getItem('showIntro');
      });
      print("ShowIntro in home");
      print(showIntro);
    } else {
      updateShowIntroInLocalStorage();
    }
  }

  void getShowIntroFromLocalStorage() async {
    if (await storage.ready) {
      print("localstorage is ready.");
      if (storage.getItem('showIntro') == null) {
        await storage.setItem('showIntro', true);
        print("1st time to set showIntro value.");
      }
      setState(() {
        showIntro = storage.getItem('showIntro');
      });
      print(storage.getItem('showIntro'));
    } else {
      print("localstorage is 'not' ready.");
      getShowIntroFromLocalStorage();
    }
  }

  // void setPageState(String state) {
  //   setState(() {
  //     pageState = state;
  //   });
  // }
}
