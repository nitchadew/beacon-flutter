import 'package:flutter/material.dart';
import 'dart:async';
// import 'dart:math' as math;

// import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:myapp/pages/Morprom/BeaconIntro.dart';
import 'package:myapp/pages/Morprom/BeaconPermissionModal.dart';
import 'package:empty_widget/empty_widget.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:myapp/pages/BeaconLib/InetBeacon.dart';
import 'package:myapp/pages/BeaconLib/BeaconResultObject.dart';

class BeaconCheckin extends StatefulWidget {
  bool showIntro;
  Function updateShowIntroInLocalStorage;
  BeaconCheckin({Key key, this.showIntro, this.updateShowIntroInLocalStorage})
      : super(key: key);
  @override
  BeaconCheckinState createState() => BeaconCheckinState();
}

class BeaconCheckinState extends State<BeaconCheckin> {
  bool animate = false;
  double widgetOpacity = 0.0;
  InetBeacon inb =
      new InetBeacon("VISIE", "5991e161-bb46-432f-9bd8-b271f76f67d9", 50000);
  Timer updateBeaconResultTimer;
  Timer updateBeaconResultWidgetTimer;
  List<BeaconResultObject> beaconResultList = [];
  List<Widget> beaconResultWidgetList = [];
  bool isBeaconScanTimeout = false;
  int scanTimeout = 10;
  int scanTimeoutCount;
  Map<String, String> selectedBeacon = {
    "name": "",
    "major": "",
    "minor": "",
  };

  @override
  void initState() {
    // aes();
    checkPermission();

    Timer(Duration(milliseconds: 200), () {
      // widget1Opacity = 1;
      setState(() {
        animate = true;
      });
    });

    Timer(Duration(milliseconds: 650), () {
      setState(() {
        widgetOpacity = 1;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        stopScan();
        return true;
      },
      child: widget.showIntro == true
          ? AnimatedOpacity(
              opacity: widgetOpacity,
              duration: Duration(milliseconds: 500),
              child: Padding(
                  padding: EdgeInsets.only(top: 25),
                  child: BeaconIntro(
                      showIntroUpdate, widget.updateShowIntroInLocalStorage)),
            )
          : beaconCheckinWidget(),
    );
  }

  Widget beaconCheckinWidget() {
    return Container(
      // color: Colors.blue,
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            flex: 2,
            child: Row(
              children: [
                Text('เลือกสถานที่เพื่อทำการเช็กอิน',
                    style: TextStyle(
                        // fontFamily: GoogleFonts.prompt(textStyle: TextStyle())
                        //     .fontFamily,
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        color: Colors.black)),
                !isBeaconScanTimeout
                    ? Padding(
                        padding: EdgeInsets.only(left: 12.0),
                        child: SizedBox(
                          child: CircularProgressIndicator(
                            strokeWidth: 2.5,
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Theme.of(context).colorScheme.onPrimary),
                          ),
                          height: 22.0,
                          width: 22.0,
                        ),
                      )
                    : Spacer()
              ],
            ),
          ),
          Expanded(
            flex: 45,
            child: !isBeaconScanTimeout
                ? Padding(
                    padding: const EdgeInsets.only(top: 35, bottom: 20),
                    child: ListView.builder(
                        //remove scroll effect
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: beaconResultWidgetList.length,
                        itemBuilder: (context, index) {
                          return beaconResultWidgetList[index];
                        }),
                  )
                : EmptyWidget(
                    hideBackgroundAnimation: true,
                    // image: 'images/visie_location.png',
                    packageImage: PackageImage.Image_1,
                    title: 'ไม่พบสถานที่',
                    subTitle: 'กรุณากดค้นหาอีกครั้ง',
                    titleTextStyle: TextStyle(
                      fontSize: 19,
                      color: Color(0xff9da9c7),
                      fontWeight: FontWeight.w600,
                    ),
                    subtitleTextStyle: TextStyle(
                      fontSize: 14,
                      color: Color(0xffabb8d6),
                    ),
                  ),
          ),
          Expanded(
              flex: 4,
              child: Container(
                // color: Colors.red,
                width: MediaQuery.of(context).size.width * 1,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).colorScheme.surface,
                    onPrimary: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)),
                  ),
                  onPressed: !isBeaconScanTimeout
                      ? selectedBeacon['name'] == ""
                          ? null
                          : () {
                              print(selectedBeacon['name']);
                            }
                      : () {
                          startScan();
                        },
                  child: !isBeaconScanTimeout
                      ? Text('เช็กอิน',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: Colors.white))
                      : Text('ค้นหาใหม่',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: Colors.white)),
                ),
              ))
        ],
      ),
    );
  }

  Widget beaconResultWidget(
      String name, String major, String minor, String rssi, bool selected) {
    return Container(
      // color: Colors.red,
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              selectedBeacon = {
                "name": name,
                "major": major,
                "minor": minor,
              };
              print(selectedBeacon);
            },
            child: Row(
              children: [
                SizedBox(
                  height: 28,
                  child: Image.asset(
                    'images/icon_beacon.png',
                  ),
                ),
                Flexible(
                  child: Padding(
                    padding: EdgeInsets.only(left: 8),
                    child: selected
                        ? Text(name,
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                color: Colors.red))
                        : Text(name,
                            style: TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: 18,
                                color: Colors.black)),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12, bottom: 12),
            child: Divider(
              thickness: 1.3,
              color: Color(0xffBDBDBD),
            ),
          ),
        ],
      ),
    );
  }

  void addBeaconResultWidgetList(
      String name, String major, String minor, String rssi, bool selected) {
    setState(() {
      beaconResultWidgetList
          .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
      // beaconResultWidgetList
      //     .add(beaconResultWidget(name, major, minor, rssi, selected));
    });
  }

  void checkPermission() async {
    bool isLocationReady = await InetBeacon.isLocationReady();
    bool isBluetoothReady = await InetBeacon.isBluetoothReady();

    if (!isBluetoothReady && !isLocationReady) {
      print("1");
      Timer(Duration(milliseconds: 1000), () {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return BeaconPermissionModal(
                  false, false, startScan, showIntroUpdate);
            });
      });
    } else if (!isBluetoothReady && isLocationReady) {
      print("11");
      Timer(Duration(milliseconds: 1000), () {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return BeaconPermissionModal(
                  false, true, startScan, showIntroUpdate);
            });
      });
    } else if (isBluetoothReady && !isLocationReady) {
      print("111");
      Timer(Duration(milliseconds: 1000), () {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return BeaconPermissionModal(
                  true, false, startScan, showIntroUpdate);
            });
      });
    } else if (isBluetoothReady && isLocationReady) {
      print("1111");
      startScan();
    }
  }

  void startScan() {
    inb.startScan();
    setState(() {
      scanTimeoutCount = 0;
      isBeaconScanTimeout = false;
    });

    //update ui
    updateBeaconResultWidgetTimer =
        Timer.periodic(const Duration(milliseconds: 100), (timer) {
      beaconResultWidgetList = [];
      bool wasSelected = false;

      // beaconResultList.asMap().forEach((index, item) {});

      //auto select same old beacon if it is still remain in the list.
      for (var item in beaconResultList) {
        if (item.minor == selectedBeacon['minor']) {
          addBeaconResultWidgetList(
              item.name, item.major, item.minor, item.rssi, true);
          selectedBeacon = {
            "name": item.name,
            "major": item.major,
            "minor": item.minor,
          };
          wasSelected = true;
        } else {
          addBeaconResultWidgetList(
              item.name, item.major, item.minor, item.rssi, false);
        }
      }

      //auto select 1st beacon in list if no beacon selected or same old is gone.
      if (!wasSelected && beaconResultList.length > 0) {
        selectedBeacon = {
          "name": beaconResultList[0].name,
          "major": beaconResultList[0].major,
          "minor": beaconResultList[0].minor,
        };
      }

      //clear selected beacon if beacon list is empty.
      if (beaconResultList.length == 0 && isBeaconScanTimeout == false) {
        selectedBeacon = {
          "name": "",
          "major": "",
          "minor": "",
        };
      }
    });

    //update scan result list
    updateBeaconResultTimer =
        Timer.periodic(const Duration(milliseconds: 1000), (timer) async {
      try {
        beaconResultList = inb.getBeaconList();
        if (beaconResultList.length == 0) {
          scanTimeoutCount++;
          if (scanTimeoutCount > scanTimeout) {
            print("time out!");
            setState(() {
              isBeaconScanTimeout = true;
            });
            stopScan();
          }
          // print("[]");
        } else {
          scanTimeoutCount = 0;
          // for (var item in beaconResultList) {
          //   item.showValue();
          // }
        }
      } on Exception catch (e) {
        print(e);
        print("No internet.");
        stopScan();
      }
    });
  }

  void stopScan() {
    inb.stopScan();
    updateBeaconResultTimer.cancel();
    updateBeaconResultWidgetTimer.cancel();
    // print('stopScan');
  }

  void showIntroUpdate(bool value) {
    // print(value);
    if (value) {
      setState(() {
        widget.showIntro = true;
      });
      Timer(Duration(milliseconds: 500), () {
        setState(() {
          widgetOpacity = 1;
        });
      });
    } else {
      setState(() {
        widgetOpacity = 0;
      });
      Timer(Duration(milliseconds: 500), () {
        setState(() {
          widget.showIntro = false;
        });
      });
    }
  }

  void aes() {
    final plainText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit';
    final testText =
        'XdjH0Ozew78EFTq+jnBR71Sv1Dtr7JFE6N3mJjPQPP2CJPKiFCMjhiimGPk/6+ngSq2hx2hID2ROU1UgqJU5490c2SJiboWrciNFzDKc+9Xw5GXC1GV/S+adNLwdKJb/ea6EXcdvGN/C3peX03BqSdkfD195uHFV+CrOU5zcKM/YyQYUXpukte1ud7omQVeE';
    final key = encrypt.Key.fromUtf8('39323334353637303100000000000000');
    final iv = encrypt.IV.fromLength(16);

    print(key.length);

    final encrypter = encrypt.Encrypter(encrypt.AES(key));

    final testEncrypt = encrypt.Encrypted.fromUtf8(testText);

    final encrypted = encrypter.encrypt(plainText, iv: iv);
    final decrypted = encrypter.decrypt(testEncrypt, iv: iv);

    print(decrypted);
  }
}
