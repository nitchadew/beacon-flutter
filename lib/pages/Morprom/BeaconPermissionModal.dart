import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:myapp/pages/BeaconLib/InetBeacon.dart';
import 'dart:async';

class BeaconPermissionModal extends StatefulWidget {
  Function startScan;
  //it's may not use
  Function showIntroUpdate;
  bool isBluetoothReady;
  bool isLocationReady;
  BeaconPermissionModal(this.isBluetoothReady, this.isLocationReady,
      this.startScan, this.showIntroUpdate);
  @override
  State<StatefulWidget> createState() => BeaconPermissionModalState();
}

class BeaconPermissionModalState extends State<BeaconPermissionModal>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  Timer timerCheckPermission, timerUpdatePermission;

  @override
  void initState() {
    super.initState();

    checkPermission();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 350));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.easeInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        int count = 0;
        timerCheckPermission.cancel();
        timerUpdatePermission.cancel();
        Navigator.of(context).popUntil((_) => count++ >= 2);
        return true;
      },
      child: ScaleTransition(
        scale: scaleAnimation,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/beacon_permission_background.png'),
                fit: BoxFit.fill),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                Spacer(
                  flex: 13,
                ),
                Expanded(
                  flex: 28,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 150,
                        child: Image.asset('images/beacon_permission_1.png'),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15),
                        child: Text('ไม่สามารถใช้งาน Beacon ได้',
                            style: TextStyle(
                                // fontFamily: GoogleFonts.prompt(textStyle: TextStyle())
                                //     .fontFamily,
                                fontWeight: FontWeight.w600,
                                fontSize: 19,
                                color: Colors.black)),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Text(
                          'หมอพร้อมจะใช้ข้อมูลตำแหน่งที่ตั้งและบลูทูธ\nเฉพาะตอนค้นหาสัญญาณ เพื่อเช็กอินและใช้งานอื่นๆ\nบนแอปพลิเคชั่นเท่านั้น ไม่มีการติดตามข้อมูลบุคคล',
                          style: TextStyle(
                              // fontFamily: GoogleFonts.prompt(textStyle: TextStyle())
                              //     .fontFamily,
                              fontWeight: FontWeight.w300,
                              fontSize: 17,
                              color: Colors.black),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
                Spacer(
                  flex: 2,
                ),
                Expanded(
                  flex: 13,
                  child: Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      color:
                          Color.fromARGB(255, 255, 255, 255).withOpacity(0.6),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        !widget.isBluetoothReady
                            ? Padding(
                                padding: EdgeInsets.only(top: 0),
                                child: Row(
                                  children: [
                                    SizedBox(
                                      height: 35,
                                      child: Image.asset(
                                          'images/beacon_permission_icon_1.png'),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 8),
                                      child: Text('กรุณาเปิดบลูทูธ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w300,
                                              fontSize: 17,
                                              color: Colors.black54)),
                                    )
                                  ],
                                ),
                              )
                            : Padding(
                                padding: EdgeInsets.only(top: 0),
                                child: Row(
                                  children: [
                                    SizedBox(
                                      height: 35,
                                      child: Image.asset(
                                          'images/beacon_permission_icon_2.png'),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 8),
                                      child: Text('เปิดบลูทูธแล้ว',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w300,
                                              fontSize: 17,
                                              color: Colors.black54)),
                                    )
                                  ],
                                ),
                              ),
                        !widget.isLocationReady
                            ? Padding(
                                padding: EdgeInsets.only(top: 0),
                                child: Row(
                                  children: [
                                    SizedBox(
                                      height: 35,
                                      child: Image.asset(
                                          'images/beacon_permission_icon_1.png'),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 8),
                                      child: Text('กรุณาเปิดตำแหน่งที่ตั้ง',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w300,
                                              fontSize: 17,
                                              color: Colors.black54)),
                                    )
                                  ],
                                ),
                              )
                            : Padding(
                                padding: EdgeInsets.only(top: 0),
                                child: Row(
                                  children: [
                                    SizedBox(
                                      height: 35,
                                      child: Image.asset(
                                          'images/beacon_permission_icon_2.png'),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 8),
                                      child: Text('เปิดตำแหน่งที่ตั้งแล้ว',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w300,
                                              fontSize: 17,
                                              color: Colors.black54)),
                                    )
                                  ],
                                ),
                              ),
                      ],
                    ),
                  ),
                ),
                Spacer(
                  flex: 10,
                ),
                Expanded(
                    flex: 5,
                    child: Container(
                        width: MediaQuery.of(context).size.width * 1,
                        child: ElevatedButton(
                            onPressed: () async {
                              var status = await Permission.location.request();
                              print(status);
                              // if (status.isDenied) {
                              //   print("permisssion denie");
                              // }
                              // await openAppSettings();
                            },
                            child: Text('อนุญาติให้เข้าถึง',
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18,
                                    color: Colors.white)),
                            style: ButtonStyle(
                                elevation: MaterialStateProperty.all<double>(0),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Color(0xff1F9D69)),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(12.0),
                                        side: BorderSide(
                                            width: 2.5,
                                            color: Color(0xff0F955D)))))))),
                Spacer(
                  flex: 1,
                ),
                Expanded(
                    flex: 5,
                    child: Container(
                        width: MediaQuery.of(context).size.width * 1,
                        child: ElevatedButton(
                            onPressed: () {
                              timerCheckPermission.cancel();
                              timerUpdatePermission.cancel();
                              int count = 0;
                              Navigator.of(context)
                                  .popUntil((_) => count++ >= 2);
                            },
                            child: Text('กลับไปหน้าแรก',
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18,
                                    color: Color.fromARGB(183, 31, 157, 105))),
                            style: ButtonStyle(
                                elevation: MaterialStateProperty.all<double>(0),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Color(0xffFFE1BE)),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(12.0),
                                        side: BorderSide(
                                            width: 2.5,
                                            color: Color(0xff0F955D)))))))),
                Spacer(
                  flex: 4,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void checkPermission() {
    bool isLocationReady;
    bool isBluetoothReady;
    timerCheckPermission =
        Timer.periodic(const Duration(milliseconds: 1000), (timer) async {
      // print("timerCheckPermission");
      isLocationReady = await InetBeacon.isLocationReady();
      isBluetoothReady = await InetBeacon.isBluetoothReady();
      if (isLocationReady && isBluetoothReady) {
        timerCheckPermission.cancel();
        timerUpdatePermission.cancel();
        //if permission granted do startScan
        widget.startScan();
        Navigator.of(context).pop();
      }
    });

    timerUpdatePermission =
        Timer.periodic(const Duration(milliseconds: 1000), (timer) {
      // print("timerUpdatePermission");
      if (isBluetoothReady != null && isLocationReady != null) {
        setState(() {
          widget.isBluetoothReady = isBluetoothReady;
          widget.isLocationReady = isLocationReady;
        });
      }
    });
  }
}
