import 'package:introduction_screen/introduction_screen.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class BeaconIntro extends StatefulWidget {
  Function showIntroUpdate;
  Function updateShowIntroInLocalStorage;
  BeaconIntro(this.showIntroUpdate, this.updateShowIntroInLocalStorage);
  @override
  _BeaconIntroState createState() => _BeaconIntroState();
}

class _BeaconIntroState extends State<BeaconIntro> {
  final introKey = GlobalKey<IntroductionScreenState>();
  final LocalStorage storage = new LocalStorage('morprom_app');
  bool isLastPage = false;

  @override
  void initState() {
    // updateLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            flex: 22,
            child: IntroductionScreen(
              key: introKey,
              globalBackgroundColor: Colors.white,
              rawPages: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 380,
                          child: Image.asset('images/beacon_intro_1.png'),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(40, 20, 10, 0),
                      child: Text(
                          'เข้าสถานที่ได้สะดวกรวดเร็วด้วย\n“เช็กอินผ่านมือถือ”',
                          style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                              color: Colors.black)),
                    )
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 380,
                          child: Image.asset('images/beacon_intro_2.png'),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(40, 20, 10, 0),
                      child: Text(
                          'โชว์หน้าเช็กอินสำเร็จให้เจ้าหน้าที่ตรวจ\nแค่นี้ก็เสร็จเรียบร้อย!',
                          style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                              color: Colors.black)),
                    )
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 380,
                          child: Image.asset('images/beacon_intro_3.png'),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(40, 20, 10, 0),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('ใช้ง่ายเพียง',
                                style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                    color: Colors.black)),
                            Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.location_on,
                                    size: 30.0,
                                    color: Color(0XFF858585),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 8),
                                    child: Text('เปิดตำแหน่งที่ตั้ง',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w300,
                                            fontSize: 17,
                                            color: Colors.black54)),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 3),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.bluetooth,
                                    size: 30.0,
                                    color: Color(0XFF858585),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 8),
                                    child: Text('เปิดบลูทูธ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w300,
                                            fontSize: 17,
                                            color: Colors.black54)),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 3),
                              child: Row(
                                children: [
                                  SizedBox(
                                    height: 28,
                                    child: Image.asset(
                                      'images/icon_beacon.png',
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 8),
                                    child: Text('เลือกสถานที่ที่ต้องการเช็กอิน',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w300,
                                            fontSize: 17,
                                            color: Colors.black54)),
                                  )
                                ],
                              ),
                            )
                          ]),
                    )
                  ],
                ),
              ],

              onChange: (page) => _currentPage(page),
              //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
              onDone: () => _onIntroEnd(context),

              overrideNext: Padding(
                  padding: EdgeInsets.only(right: 2),
                  child: ElevatedButton(
                    onPressed: () {
                      _next();
                    },
                    child: Text("DSdsds"),
                  )),
              skipOrBackFlex: 0,
              nextFlex: 0,
              showSkipButton: false,
              showDoneButton: false,
              showNextButton: false,
              showBackButton: false,
              back: const Icon(Icons.arrow_back),
              skip: const Text('Skip',
                  style: TextStyle(fontWeight: FontWeight.w600)),
              next: const Icon(Icons.arrow_forward),
              done: const Text('Done',
                  style: TextStyle(fontWeight: FontWeight.w600)),
              curve: Curves.fastLinearToSlowEaseIn,
              controlsMargin: const EdgeInsets.all(16),
              dotsDecorator: const DotsDecorator(
                size: Size(10.0, 10.0),
                color: Color(0xFFBDBDBD),
                activeSize: Size(10.0, 10.0),
                activeColor: Color(0xFF0F955D),
                activeShape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(25.0)),
                ),
              ),
              dotsContainerDecorator: const ShapeDecoration(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                ),
              ),
            )),
        Expanded(
            flex: 2,
            child: Container(
                padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                width: MediaQuery.of(context).size.width * 1,
                child: isLastPage
                    ? ElevatedButton(
                        onPressed: () {
                          //set ShowIntro in localstoreage to false
                          widget.updateShowIntroInLocalStorage();
                          _onIntroEnd(context);
                        },
                        child: Text('เริ่มต้นใช้งาน',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                color: Colors.white)),
                        style: ButtonStyle(
                            elevation: MaterialStateProperty.all<double>(0),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Color(0xff1F9D69)),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                    side: BorderSide(
                                        width: 2.5,
                                        color: Color(0xff0F955D))))))
                    : ElevatedButton(
                        onPressed: () {
                          _next();
                        },
                        child: Text('ถัดไป',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                color: Color(0xff1F9D69))),
                        style: ButtonStyle(
                            elevation: MaterialStateProperty.all<double>(0),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Color.fromARGB(255, 255, 255, 255)),
                            shape:
                                MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0), side: BorderSide(width: 2.5, color: Color(0xff0F955D)))))))),
        Spacer(
          flex: 1,
        )
      ],
    );
  }

  void _onIntroEnd(context) {
    widget.showIntroUpdate(false);
  }

  void _currentPage(page) {
    if (page == introKey.currentState.getPagesLength() - 1) {
      setState(() {
        isLastPage = true;
      });
    } else {
      setState(() {
        isLastPage = false;
      });
    }
    // print(introKey.currentState.getPagesLength());
    // print(page);
  }

  void _next() {
    introKey.currentState.controller
        .nextPage(duration: Duration(milliseconds: 250), curve: Curves.easeIn);
  }

  // void updateLocalStorage() async {
  //   if (await storage.ready) {
  //     await storage.setItem('showIntro', false);
  //     print("updateLocalStorage");
  //     print(storage.getItem('showIntro'));
  //   } else {
  //     updateLocalStorage();
  //   }
  // }
}
