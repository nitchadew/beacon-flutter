import 'package:flutter/material.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:localstorage/localstorage.dart';
import 'package:empty_widget/empty_widget.dart';

import 'dart:async';

import 'package:myapp/pages/BeaconLib/InetBeacon.dart';
import 'package:myapp/pages/BeaconLib/BeaconResultObject.dart';

class MyModal extends StatefulWidget {
  @override
  _MyModalState createState() => _MyModalState();
}

class _MyModalState extends State<MyModal> {
  final LocalStorage storage = new LocalStorage('localstorage_app');

  InetBeacon inb =
      new InetBeacon("VISIE", "5991e161-bb46-432f-9bd8-b271f76f67d9", 50000);
  bool wasSelected = false;
  Map<String, String> selectedBeacon = {
    "name": "",
    "major": "",
    "minor": "",
  };
  bool _loading = false;
  bool wasFound = false;
  List<BeaconResultObject> myList = [];
  List<Widget> _beaconLists = [];
  Timer updateBeaconCardTimer;
  Timer beaconResultTimer;
  int scanTimeout = 20;
  int scanTimeoutCount = 0;
  bool isTimeout = false;

  @override
  void initState() {
    _startScan();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
          width: MediaQuery.of(context).size.width - 55,
          height: 600,
          child: Stack(
            children: [
              Positioned(
                right: 0.0,
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    _stopScan();
                    print("exit");
                  },
                  child: Align(
                    alignment: Alignment.topRight,
                    child: CircleAvatar(
                      radius: 22.0,
                      backgroundColor: Colors.white,
                      child: Icon(Icons.close, color: Colors.red),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: 27.0, bottom: 10.0, left: 15.0, right: 15.0),
                child: Column(
                  // mainAxisSize: MainAxisSize.min,
                  // mainAxisAlignment: MainAxisAlignment.center,
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ListTile(
                      leading: SizedBox.fromSize(
                        size: Size.fromRadius(25),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.surface,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Icon(
                            Icons.location_on,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      title: Text("ขออนุญาติเข้าถึงตำแหน่งของคุณ"),
                      subtitle: Text(
                        'กรุณาเปิดการใช้งาน Bluetooth และ Location บนอุปกรณ์ของคุณเพื่อเข้าใช้งานเช็คอิน',
                        style: new TextStyle(
                          fontSize: 11.5,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: 25.0,
                      ),
                      child: ListTile(
                        title: Text("สถานที่:"),
                      ),
                    ),
                    _loading
                        ? Padding(
                            padding: EdgeInsets.only(top: 52.0),
                            child: SizedBox(
                              child: CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Theme.of(context).colorScheme.surface),
                              ),
                              height: 65.0,
                              width: 65.0,
                            ),
                          )
                        : SizedBox(
                            height: 350,
                            child: !isTimeout
                                ? ListView.builder(
                                    //remove scroll effect
                                    physics: BouncingScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: _beaconLists.length,
                                    itemBuilder: (context, index) {
                                      return Padding(
                                          padding: EdgeInsets.only(bottom: 5.0),
                                          child: _beaconLists[index]);
                                    })
                                : _beaconCardEmpty(),
                          ),
                  ],
                ),
              ),
              Positioned(
                child: Padding(
                    padding: EdgeInsets.only(bottom: 20.0),
                    child: Align(
                        alignment: FractionalOffset.bottomCenter,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Theme.of(context).colorScheme.surface,
                              onPrimary: Colors.white,
                              minimumSize: Size(200, 42),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                            ),
                            onPressed:
                                selectedBeacon['name'] == "" ? null : _checkIn,
                            child: Text('เช็คอิน')))),
              )
            ],
          )),
    );
  }

  void _checkIn() {
    // print(storage.getItem('uid'));
    if (storage.getItem('uid') == null || storage.getItem('uid') == "") {
      CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          confirmBtnColor: Colors.green,
          confirmBtnText: 'ปิด',
          confirmBtnTextStyle: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w400, fontSize: 14.0),
          title: 'เช็คอินไม่สำเร็จ',
          text: 'กรุณากรอกเลขบัตรประชาชน');
    } else {
      CoolAlert.show(
        context: context,
        type: CoolAlertType.success,
        confirmBtnColor: Colors.green,
        confirmBtnText: 'ปิด',
        confirmBtnTextStyle: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w400, fontSize: 14.0),
        title: 'เช็คอินสำเร็จ',
        text: "สถานที่: " +
            selectedBeacon['name'] +
            "\n" +
            "เลขบัตรประชาชน: " +
            storage.getItem('uid'),
      );
    }
  }

  void _stopScan() {
    myList = [];
    inb.stopScan();
    if (beaconResultTimer != null) {
      beaconResultTimer.cancel();
    }
    if (updateBeaconCardTimer != null) {
      updateBeaconCardTimer.cancel();
    }
    setState(() {
      _loading = false;
    });
    print("stoped.");
  }

  void _startScan() async {
    scanTimeoutCount = 0;
    setState(() {
      isTimeout = false;
      _loading = true;
    });

    //update ui
    updateBeaconCardTimer =
        Timer.periodic(const Duration(milliseconds: 100), (timer) {
      _beaconLists = [];
      wasSelected = false;
      myList.asMap().forEach((index, item) {
        if (item.minor == selectedBeacon['minor']) {
          _addBeaconCardList(
              item.name, item.major, item.minor, item.rssi, true);
          selectedBeacon = {
            "name": item.name,
            "major": item.major,
            "minor": item.minor,
          };
          wasSelected = true;
        } else {
          _addBeaconCardList(
              item.name, item.major, item.minor, item.rssi, false);
        }
      });
      if (!wasSelected && myList.length > 0) {
        selectedBeacon = {
          "name": myList[0].name,
          "major": myList[0].major,
          "minor": myList[0].minor,
        };
      }
      if (myList.length == 0 && isTimeout == false) {
        setState(() {
          _loading = true;
        });
        selectedBeacon = {
          "name": "",
          "major": "",
          "minor": "",
        };
      }
    });

    //update scan result
    inb.startScan();
    beaconResultTimer =
        Timer.periodic(const Duration(milliseconds: 1000), (timer) async {
      try {
        myList = inb.getBeaconList();
        // print("catch result");
        if (myList.length == 0) {
          scanTimeoutCount++;
          if (scanTimeoutCount > scanTimeout) {
            setState(() {
              isTimeout = true;
              _loading = false;
            });
            _stopScan();
          }
          // print("[]");
        } else {
          scanTimeoutCount = 0;
          // for (var item in myList) {
          //   item.showValue();
          // }
        }
      } on Exception catch (e) {
        print(e);
        Navigator.of(context).pop();
        CoolAlert.show(
          backgroundColor: Colors.teal,
          context: context,
          type: CoolAlertType.warning,
          title: 'ไม่สามารถเชื่อมต่ออินเทอร์เน็ต',
          text: 'กรุณาเปิดการใช้งานอินเทอร์เน็ตของคุณเพื่อเช็คอินสถานที่',
        );
        _stopScan();
      }
    });
  }

  void _addBeaconCardList(
      String _name, String _major, String _minor, String _rssi, bool selected) {
    setState(() {
      _loading = false;
      _beaconLists.add(_beaconCard(_name, _major, _minor, _rssi, selected));
    });
  }

  Widget _beaconCard(String _name, String _major, String _minor, String _rssi,
      bool _selected) {
    final String name = _name;
    final String major = _major;
    final String minor = _minor;
    final String rssi = _rssi;
    return GestureDetector(
      onTap: () {
        selectedBeacon = {
          "name": name,
          "major": major,
          "minor": minor,
        };
      },
      child: Padding(
        padding: EdgeInsets.only(
          top: 0.0,
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 8.0, right: 8.0),
          child: Container(
            decoration: BoxDecoration(
              color: _selected
                  ? Theme.of(context).colorScheme.surface
                  : Colors.grey.shade200,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: ListTile(
              leading: SizedBox.fromSize(
                size: Size.fromRadius(25),
                child: Image.asset('images/visie_location.png'),
              ),
              title: _selected
                  ? Text("$_name" + ": " + "$_major" + "/" + "$_minor",
                      style: TextStyle(color: Colors.white))
                  : Text("$_name" + ": " + "$_major" + "/" + "$_minor",
                      style: TextStyle(color: Colors.black)),
            ),
          ),
        ),
      ),
    );
  }

  Widget _beaconCardEmpty() {
    return Container(
      transform: Matrix4.translationValues(0.0, -60.0, 0.0),
      child: SizedBox(
          width: 330.0,
          height: 330.0,
          child: Stack(children: [
            EmptyWidget(
              hideBackgroundAnimation: true,
              // image: 'images/visie_location.png',
              packageImage: PackageImage.Image_3,
              title: 'ไม่พบสถานที่',
              subTitle: 'กรุณากดค้นหาอีกครั้ง',
              titleTextStyle: TextStyle(
                fontSize: 19,
                color: Color(0xff9da9c7),
                fontWeight: FontWeight.w600,
              ),
              subtitleTextStyle: TextStyle(
                fontSize: 14,
                color: Color(0xffabb8d6),
              ),
            ),
            Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Color.fromARGB(255, 138, 156, 201),
                          onPrimary: Colors.white,
                          minimumSize: Size(100, 40),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                        ),
                        onPressed: _startScan,
                        child: Text('ค้นหา'))))
          ])),
    );
  }
}
