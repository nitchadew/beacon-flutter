import 'package:flutter/material.dart';
import 'package:flutter_beacon/flutter_beacon.dart';
import 'dart:async';

import 'package:myapp/pages/BeaconLib/InetBeacon.dart';

class MyBeaconScan extends StatefulWidget {
  const MyBeaconScan({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyBeaconScanState createState() => _MyBeaconScanState();
}

class _MyBeaconScanState extends State<MyBeaconScan> {
  String _showText = "Not Found Any Beacon.";
  String _scanButtText = "Start Scan";
  bool _loading = false;
  bool wasFound = false;
  List<Beacon> myList = [];
  List<Widget> _beaconCardList = [];
  Timer timer;
  Timer beaconResultTimer;

  InetBeacon inb =
      new InetBeacon("VISIE", "5991e161-bb46-432f-9bd8-b271f76f67d9", 30000);

  void _addBeaconCardList(
      String _name, String _major, String _minor, String _rssi) {
    setState(() {
      _loading = false;
      _beaconCardList.add(_beaconCard(_name, _major, _minor, _rssi));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            Positioned(
                child: Align(
                    alignment: FractionalOffset.center,
                    child: _loading
                        ? SizedBox(
                            child: CircularProgressIndicator(
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                  Colors.green),
                            ),
                            height: 80.0,
                            width: 80.0,
                          )
                        : _beaconCardList.isEmpty
                            ? Text(
                                '$_showText',
                                style: Theme.of(context).textTheme.headline5,
                              )
                            : Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: 14.0, horizontal: 10.0),
                                child: ListView.builder(
                                    itemCount: _beaconCardList.length,
                                    itemBuilder: (context, index) {
                                      return _beaconCardList[index];
                                    }),
                              ))),
            Positioned(
              child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: Container(
                    margin: const EdgeInsets.all(30.0),
                    child: ListTile(
                      title: Row(
                        children: [
                          Expanded(
                              child: Container(
                            margin: const EdgeInsets.all(10.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.green, // background
                                onPrimary: Colors.white, // foreground
                              ),
                              onPressed: testStart,
                              child: Text('$_scanButtText'),
                            ),
                          )),
                          Expanded(
                              child: Container(
                            margin: const EdgeInsets.all(10.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.red, // background
                                onPrimary: Colors.white, // foreground
                              ),
                              onPressed: testStop,
                              child: Text('Stop Scan'),
                            ),
                          ))
                        ],
                      ),
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _beaconCard(String _name, String _major, String _minor, String _rssi) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 0.0),
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              leading: SizedBox.fromSize(
                size: Size.fromRadius(18),
                child: FittedBox(
                  child: Icon(Icons.bluetooth_rounded),
                ),
              ),
              title: Text(_name),
              subtitle: Text('Major: $_major , Minor: $_minor , Rssi: $_rssi'),
            ),
          ],
        ),
      ),
    );
  }

  void testStart() {
    inb.startScan();
    beaconResultTimer =
        Timer.periodic(const Duration(milliseconds: 1000), (timer) async {
      _beaconCardList = [];
      var beaconList = inb.getBeaconList();
      for (var item in beaconList) {
        // item.showValue();
        _addBeaconCardList("บ่ได้ยิง", item.major, item.minor, item.rssi);
      }
    });
  }

  void testStop() {
    if (beaconResultTimer != null) {
      beaconResultTimer.cancel();
    }

    inb.stopScan();
  }
}
