import 'package:flutter/material.dart';
import 'package:myapp/pages/Test/MyModal.dart';
import 'package:localstorage/localstorage.dart';
import 'package:cool_alert/cool_alert.dart';

import 'dart:async';

class MyBeaconHome extends StatefulWidget {
  MyBeaconHome({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyBeaconHomeState createState() => _MyBeaconHomeState();
}

class _MyBeaconHomeState extends State<MyBeaconHome> {
  final LocalStorage storage = new LocalStorage('localstorage_app');
  TextEditingController controller;

  Timer textfieldTimer;

  @override
  void initState() {
    controller = TextEditingController(text: null);
    textfieldTimer = Timer.periodic(const Duration(milliseconds: 200), (timer) {
      if (storage.getItem('uid') != null) {
        setState(() {
          // controller = TextEditingController(text: storage.getItem('uid'));
          controller.text = storage.getItem('uid');
        });
        textfieldTimer.cancel();
        print("catch uid: " + storage.getItem('uid'));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
          backgroundColor: Theme.of(context).colorScheme.background,
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            backgroundColor: Theme.of(context).colorScheme.primary,
            elevation: 0,
            title: Row(children: [
              // SizedBox.fromSize(
              //   size: Size.fromRadius(22),
              //   child: Image.asset(
              //     'images/visie_logo.png',
              //   ),
              // ),
              Text(
                widget.title,
                style: TextStyle(
                    fontSize: 19.5,
                    color: Colors.black54,
                    fontWeight: FontWeight.w900),
              )
            ]),
          ),
          body: Container(
            child: Stack(
              children: [
                Positioned(
                  child: Padding(
                      padding: EdgeInsets.only(top: 30.0, left: 50, right: 50),
                      child: Column(
                        children: [
                          TextField(
                            maxLength: 13,
                            controller: controller,
                            onChanged: (content) {
                              setState(() {
                                controller.text = content;
                                controller.selection =
                                    TextSelection.fromPosition(TextPosition(
                                        offset: controller.text.length));
                              });
                            },
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(top: 0),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: BorderSide(
                                      color: Colors.teal.shade500, width: 1.0)),
                              filled: true,
                              fillColor: Colors.teal.shade50,
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Color(0xff038951)),
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              hintText: 'กรุณากรอกเลขบัตรประชาชน',
                              labelText: 'เลขบัตรประชาชน',
                              prefixIcon: Icon(
                                Icons.person,
                                color: Theme.of(context).colorScheme.surface,
                              ),
                              prefixText: ' ',
                              suffixIcon: Container(
                                  height: 50,
                                  child: GestureDetector(
                                    onTap: () {
                                      controller.text = "";
                                      storage.deleteItem('uid');
                                      print("click");
                                    },
                                    child: Material(
                                      // elevation: 5.0,
                                      color:
                                          Theme.of(context).colorScheme.surface,
                                      borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(15.0),
                                        bottomRight: Radius.circular(15.0),
                                      ),
                                      child: Icon(Icons.delete,
                                          color: Colors.white, size: 22.0),
                                    ),
                                  )),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 0.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Theme.of(context).colorScheme.surface,
                                onPrimary: Colors.white,
                                minimumSize: Size(115, 45),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                              ),
                              onPressed: controller.text == "" ||
                                      controller.text == null ||
                                      controller.text ==
                                          storage.getItem('uid') ||
                                      controller.text.length < 13
                                  ? null
                                  : () {
                                      storage.setItem('uid', controller.text);
                                      print(storage.getItem('uid'));
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                      CoolAlert.show(
                                        backgroundColor: Colors.teal,
                                        context: context,
                                        type: CoolAlertType.success,
                                        title: 'บันทึกสำเร็จ',
                                        text: 'เลขบัตรประชาชนได้ถูกบันทึกแล้ว',
                                        confirmBtnColor: Colors.green,
                                        confirmBtnText: 'ปิด',
                                        confirmBtnTextStyle: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14.0),
                                      );
                                    },
                              child: Text('บันทึก'),
                            ),
                          ),
                        ],
                      )),
                ),
                Positioned(
                  child: Padding(
                      padding: EdgeInsets.only(top: 0.0),
                      child: Align(
                          alignment: FractionalOffset.center,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Theme.of(context).colorScheme.surface,
                              onPrimary: Colors.white,
                              minimumSize: Size(80, 80),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1000.0)),
                            ),
                            onPressed: dialogBuild,
                            child: Icon(
                              Icons.location_on,
                              color: Colors.white,
                              size: 35.0,
                            ),
                          ))),
                ),
                Positioned(
                  child: Padding(
                      padding: EdgeInsets.only(bottom: 0.0),
                      child: Align(
                          alignment: FractionalOffset.bottomCenter,
                          child: Container(
                            height: 40,
                            decoration: BoxDecoration(
                              // borderRadius: BorderRadius.only(
                              //   topRight: Radius.circular(35),
                              //   topLeft: Radius.circular(35),
                              // ),
                              // boxShadow: [
                              //   BoxShadow(
                              //     color: Theme.of(context)
                              //         .colorScheme
                              //         .primary
                              //         .withOpacity(1),
                              //     spreadRadius: 10,
                              //     blurRadius: 10,
                              //     offset: Offset(
                              //         0, 0), // changes position of shadow
                              //   ),
                              // ],
                              color: Theme.of(context).colorScheme.primary,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('Version 1.2.0',
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 13))
                              ],
                            ),
                          ))),
                )
              ],
            ),
          ),
        ));
  }

  void dialogBuild() {
    showGeneralDialog(
        // barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
              opacity: a1.value,
              child: MyModal(),
            ),
          );
        },
        transitionDuration: Duration(milliseconds: 270),
        barrierDismissible: false,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {});
  }

  // void dialogBuild() {
  //   FocusScope.of(context).requestFocus(new FocusNode());
  //   showDialog(
  //       barrierDismissible: false,
  //       context: context,
  //       builder: (BuildContext context) {
  //         return MyModal();
  //       });
  // }
}
